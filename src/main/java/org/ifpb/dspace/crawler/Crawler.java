package org.ifpb.dspace.crawler;

import java.io.IOException;
import java.sql.SQLException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.ifpb.dspace.persistence.*;

public class Crawler {

	public static final String URL = "http://www.lume.ufrgs.br";

	public static void persistirEntidades() {
		// TODO Auto-generated method stub

		
		Persistence.connect();
		Document docMain;
		
		try {
			docMain = Jsoup.connect("http://www.lume.ufrgs.br/").get();
			Elements elements = docMain.select("a[class]");
			for (Element link : elements) {
				if (link.attr("class").contains("titulos_comunidades")
						&& !(link.text().contains("Acervos") || link.text()
								.contains("Eventos"))) {

					System.out.println("Nome da comunidade: " + link.text());
					int community_id = Persistence.addCommunity(link.text());
					if (community_id >= 1) {
						Document doc1 = Jsoup.connect(URL + link.attr("href"))
								.get();
						persistirEntidades(doc1, "", community_id);
					} else {
						System.out.println("Erro ao persistir comunidade");
					}

				}
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	private static void persistirEntidades(Document doc, String opt,
			int community_id) throws IOException {
		Elements links = doc.select("div[class=artifact-title, Z3988] > a");
		opt = opt + "-";
		if (links.hasText()) {
			for (Element link : links) {
				System.out.println(opt + "Nome da subcomunidade: "
						+ link.text());
				int subcommunity_id = Persistence.addSubcommunity(community_id,
						link.text());
				
				if (community_id > 0) {
					Document docMain = Jsoup.connect(URL + link.attr("href"))
							.get();
					persistirEntidades(docMain, opt, subcommunity_id);
				} else {
					System.out.println("Erro ao adicionar subcomunidade.");
				}
			}
			return;
		}
		links = doc.select("div[class=artifact-title, nao-bold] > a");
		if (links.hasText()) {
			for (Element link : links) {
				System.out.println(opt + "Nome da coleção: " + link.text());

				try {
					Persistence.addCollection(community_id, link.text());
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}
	}
}

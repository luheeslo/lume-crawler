package org.ifpb.dspace.persistence;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.Random;

import org.ifpb.dspace.utils.RandomInteger;

public class Persistence {

	private static Connection conn = null;
	private static int suffix;
	private static int preffix;

	public static Connection connect() {

		preffix = RandomInteger.showRandomInteger(100000, 999999, new Random());
		suffix = 1;

		if (conn != null) {
			return conn;
		}

		Properties props = new Properties();
		String url = "jdbc:postgresql://localhost:5432/dspace";
		InputStream input = null;

		try {
			input = new FileInputStream("config.properties");
			props.load(input);
			Class.forName("org.postgresql.Driver");

			conn = DriverManager.getConnection(url, props);

		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return conn;

	}

	public static int addCommunity(String name) {

		int community_key = -1;
		Statement stCommunity = null;
		PreparedStatement pstHandle = null;
		PreparedStatement pstResourcePolicy = null;
		PreparedStatement pstMetadatavalues = null;
		try {
			stCommunity = conn.createStatement();

			stCommunity
					.executeUpdate(
							"INSERT INTO community (community_id) VALUES (getnextid('community'))",
							Statement.RETURN_GENERATED_KEYS);
			ResultSet rs = stCommunity.getGeneratedKeys();

			if (rs.next()) {
				community_key = rs.getInt(1);

				// add handle
				pstHandle = conn
						.prepareStatement("INSERT INTO handle ( handle_id,resource_type_id,resource_id,handle) VALUES ( getnextid('handle'),4,?,?)");
				pstHandle.setInt(1, community_key);
				pstHandle.setString(2,
						String.valueOf(preffix) + "/" + String.valueOf(suffix));
				pstHandle.executeUpdate();

				// add resource policy
				pstResourcePolicy = conn
						.prepareStatement("INSERT INTO resourcepolicy (resource_type_id,policy_id,action_id,epersongroup_id,resource_id) VALUES (4,getnextid('resourcepolicy'),0,0,?)");
				pstResourcePolicy.setInt(1, community_key);
				pstResourcePolicy.executeUpdate();

				// add metadatavalue
				pstMetadatavalues = conn
						.prepareStatement("INSERT INTO metadatavalue ( text_value,resource_type_id,confidence,metadata_value_id,resource_id,place,metadata_field_id) VALUES ( ?,4,-1,getnextid('metadatavalue'),?,1,64)");
				pstMetadatavalues.setString(1, name);
				pstMetadatavalues.setInt(2, community_key);
				pstMetadatavalues.executeUpdate();
			}
		} catch (SQLException e) {
			System.out
					.println("Erro durante a persistência da entidade Comunidade.");
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				System.out.println("Erro durante o rollback.");
				e1.printStackTrace();
			}

		} finally {

			try {
				if (stCommunity != null) {
					stCommunity.close();
				}
				if (pstHandle != null) {
					pstHandle.close();
				}
				if (pstResourcePolicy != null) {
					pstResourcePolicy.close();
				}
				if (pstMetadatavalues != null) {
					pstMetadatavalues.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		suffix += 1;
		return community_key;
	}

	public static int addSubcommunity(int community_id, String name) {

		int community_key = -1;

		Statement stCommunity = null;
		PreparedStatement pstHandle = null;
		PreparedStatement pstResourcePolicy = null;
		PreparedStatement pstMetadatavalues = null;
		PreparedStatement pstCommunity2Community = null;

		try {

			stCommunity = conn.createStatement();

			stCommunity
					.executeUpdate(
							"INSERT INTO community (community_id) VALUES (getnextid('community'))",
							Statement.RETURN_GENERATED_KEYS);
			ResultSet rs = stCommunity.getGeneratedKeys();

			if (rs.next()) {
				community_key = rs.getInt(1);
				// add handle
				pstHandle = conn
						.prepareStatement("INSERT INTO handle ( handle_id,resource_type_id,resource_id,handle) VALUES ( getnextid('handle'),4,?,?)");
				pstHandle.setInt(1, community_key);
				pstHandle.setString(2,
						String.valueOf(preffix) + "/" + String.valueOf(suffix));
				pstHandle.executeUpdate();

				// add resource policy
				pstResourcePolicy = conn
						.prepareStatement("INSERT INTO resourcepolicy (resource_type_id,policy_id,action_id,epersongroup_id,resource_id) VALUES (4,getnextid('resourcepolicy'),0,0,?)");
				pstResourcePolicy.setInt(1, community_key);
				pstResourcePolicy.executeUpdate();

				// add metadatavalue
				pstMetadatavalues = conn
						.prepareStatement("INSERT INTO metadatavalue ( text_value,resource_type_id,confidence,metadata_value_id,resource_id,place,metadata_field_id) VALUES ( ?,4,-1,getnextid('metadatavalue'),?,1,64)");
				pstMetadatavalues.setString(1, name);
				pstMetadatavalues.setInt(2, community_key);
				pstMetadatavalues.executeUpdate();

				pstCommunity2Community = conn
						.prepareStatement("INSERT INTO community2community ( parent_comm_id,id,child_comm_id) VALUES ( ?,getnextid('community2community'),?)");
				pstCommunity2Community.setInt(1, community_id);
				pstCommunity2Community.setInt(2, community_key);
				pstCommunity2Community.executeUpdate();

			}
		} catch (SQLException e) {
			System.out
					.println("Erro durante a persistência da entidade Subcommunity.");
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				System.out.println("Erro durante o rollback.");
				e1.printStackTrace();
			}
		} finally {
			try {
				if (stCommunity != null) {
					stCommunity.close();
				}
				if (pstHandle != null) {
					pstHandle.close();
				}
				if (pstResourcePolicy != null) {
					pstResourcePolicy.close();
				}
				if (pstMetadatavalues != null) {
					pstMetadatavalues.close();
				}
				if (pstCommunity2Community != null) {
					pstCommunity2Community.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		suffix += 1;
		return community_key;

	}

	public static void addCollection(int community_id, String name)
			throws SQLException {

		int collection_key = -1;

		Statement stCommunity = null;
		PreparedStatement pstHandle = null;
		PreparedStatement pstResourcePolicy = null;
		PreparedStatement pstMetadatavalues = null;
		PreparedStatement pstCommunity2Collection = null;
		try {
			
			stCommunity = conn.createStatement();
			
			stCommunity
					.executeUpdate(
							"INSERT INTO collection (collection_id) VALUES (getnextid('collection'))",
							Statement.RETURN_GENERATED_KEYS);
			ResultSet rs = stCommunity.getGeneratedKeys();
			if (rs.next()) {

				collection_key = rs.getInt(1);
				pstHandle = conn
						.prepareStatement("INSERT INTO handle ( handle_id,resource_type_id,resource_id,handle) VALUES ( getnextid('handle'),3,?,?)");
				pstHandle.setInt(1, collection_key);
				pstHandle.setString(2,
						String.valueOf(preffix) + "/" + String.valueOf(suffix));
				pstHandle.executeUpdate();

				// add resource policy
				pstResourcePolicy = conn
						.prepareStatement("INSERT INTO resourcepolicy (resource_type_id,policy_id,action_id,epersongroup_id,resource_id) VALUES (3,getnextid('resourcepolicy'),0,0,?)");
				pstResourcePolicy.setInt(1, collection_key);
				pstResourcePolicy.executeUpdate();

				// add metadatavalue
				pstMetadatavalues = conn
						.prepareStatement("INSERT INTO metadatavalue ( text_value,resource_type_id,confidence,metadata_value_id,resource_id,place,metadata_field_id) VALUES ( ?,3,-1,getnextid('metadatavalue'),?,1,64)");
				pstMetadatavalues.setString(1, name);
				pstMetadatavalues.setInt(2, collection_key);
				pstMetadatavalues.executeUpdate();

				pstCommunity2Collection = conn
						.prepareStatement("INSERT INTO community2collection ( collection_id,community_id,id) VALUES (?,?,getnextid('community2collection'))");
				pstCommunity2Collection.setInt(1, collection_key);
				pstCommunity2Collection.setInt(2, community_id);
				pstCommunity2Collection.executeUpdate();
			}
		} catch (SQLException e) {
			System.out
					.println("Erro durante a persistência da entidade Collection.");
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				System.out.println("Erro durante o rollback.");
				e1.printStackTrace();
			}
		} finally {
			try {
				if (stCommunity != null) {
					stCommunity.close();
				}
				if (pstHandle != null) {
					pstHandle.close();
				}
				if (pstResourcePolicy != null) {
					pstResourcePolicy.close();
				}
				if (pstMetadatavalues != null) {
					pstMetadatavalues.close();
				}
				if (pstCommunity2Collection != null) {
					pstCommunity2Collection.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		suffix += 1;
	}
}
